package com.example.michael.myprojectwebservices;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by michael on 08/11/2017.
 */

public class Perfil extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener{
    EditText edNombre, edApellido, edContrasenia, edUsuario;
    ProgressDialog progreso;
    JsonObjectRequest jsonObjectRequest;
    RequestQueue requestQueue;
    String id_persona;
    Boolean validarMensaje =false;
    Button btnActualizar, btnSalir;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);


        edNombre = (EditText) findViewById(R.id.etNombrea);
        edApellido = (EditText) findViewById(R.id.etApellidoa);
        edUsuario = (EditText) findViewById(R.id.etUsuarioa);
        edContrasenia = (EditText)findViewById(R.id.etContraseniaa);
        btnActualizar = (Button) findViewById(R.id.btnActualizar);


        Intent recibir = getIntent();
        String datos = recibir.getStringExtra("id");
        id_persona = datos;
        //Toast.makeText(Perfil.this, "Perfil " + "usuario con id "+  id_persona, Toast.LENGTH_SHORT).show();

        requestQueue = Volley.newRequestQueue(Perfil.this);
        cargarWebservices();
        //aquí llamo al que me muestra los datos
        //Toast.makeText(Perfil.this, "nombre"+ edNombre.getText() + " usuario "+edUsuario.getText() + " contra "+ edContrasenia.getText()+ " apellido "+ edApellido.getText()+ "  id "+ id_persona, Toast.LENGTH_SHORT).show();

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//cuando doy click en el boton actualizar..
              /*  ActualizarPersona ac = new ActualizarPersona();
                ac.cargarWerService(edNombre.getText().toString(), edUsuario.getText().toString(), edContrasenia.getText().toString(), edApellido.getText().toString(), id_persona);
*/
//              new ActualizarPersona(edNombre.getText().toString(), edUsuario.getText().toString(), edContrasenia.getText().toString(), edApellido.getText().toString(), id_persona);
               /* Intent i = new Intent(Perfil.this, PruebaActualizar.class);
                i.putExtra("datos", id_persona);*/
                //startActivity(i);}
                //Toast.makeText(Perfil.this, "id es "+id_persona, Toast.LENGTH_SHORT).show();
                cargarWebservicesActualizar();
                //llamo este otro web services..
                //cargarWebservices();
                validarMensaje=true;
            }
        });




    }

    private void cargarWebservicesActualizar() {

        //Toast.makeText(Perfil.this, "nombre"+ edNombre.getText() + " usuario "+edUsuario.getText() + " contra "+ edContrasenia.getText()+ " apellido "+ edApellido.getText()+ "  id "+ id_persona, Toast.LENGTH_SHORT).show();
        //ActualizarPersona actualizar = new ActualizarPersona(edNombre.getText().toString(), edUsuario.getText().toString(), edContrasenia.getText().toString(), edApellido.getText().toString(), this.id_persona);
        String url = "http://pruebawebser.webcindario.com/actualizarPersona.php?nombre="+edNombre.getText().toString()+"&usuario="+edUsuario.getText().toString()+"&contrasenia="+edContrasenia.getText().toString()+"&apellido="+edApellido.getText().toString()+"&id_persona="+this.id_persona;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        requestQueue.add(jsonObjectRequest);
        //y cuando todo va bien pues de nuevo me envia al onResponse...

    }

    private void cargarWebservices() {

            progreso = new ProgressDialog(Perfil.this);
            progreso.setMessage("Consultando..");
            String url = "http://pruebawebser.webcindario.com/mostrarPersona.php?id="+this.id_persona;


            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            requestQueue.add(jsonObjectRequest);
        //cuando todo va bien, pues va al onResponse..
        }


        @Override
        public void onResponse(JSONObject response) {
            System.out.println("-----------------respuesta-------------");
            System.out.println(response);
            progreso.hide();
//ahora viene lo que creo qure estoy haciendo mal...
            JSONArray json = response.optJSONArray("usuario");
            //este es el nombre de lo que traigo de php mostrarPersona "usuario"
            JSONObject jsonObject = null;
//para solucionar, lo que estaba intentando hacer era crear otra clase para el webservices actualizar..
            try {
                jsonObject = json.getJSONObject(0);
                edNombre.setText(jsonObject.getString("nombre"));
                edApellido.setText(jsonObject.getString("apellido"));
                edUsuario.setText(jsonObject.getString("usuario"));
                edContrasenia.setText(jsonObject.getString("contrasenia"));
                //---Aquí cargo todo

                //En caso de que quiera actualizar
                if(validarMensaje!=false) {
                    //y aquí hago una validacion estupida al ser actualizado... entonces me marca un error aquí

                    Toast.makeText(this, "Datos editados", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Perfil.this, ActivityPrincipal.class);
                    i.putExtra("id", this.id_persona);
                    startActivity(i);

                }

                //else {Toast.makeText(this, "Hubo un error", Toast.LENGTH_SHORT).show();}


            } catch (JSONException e) {
                e.printStackTrace();
                //Toast.makeText(getApplicationContext(),"El usuario no existe en la base de datos",Toast.LENGTH_LONG).show();

            }catch (NullPointerException e){

                Toast.makeText(getApplicationContext(),"Ha ocurrido un error NullPointer",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }



        }
        @Override
        public void onErrorResponse(VolleyError error) {
            progreso.hide();
            Toast.makeText(Perfil.this, "Ha ocurrido un error ErrorResponse", Toast.LENGTH_SHORT).show();
            Log.i("Error", error.toString());
        }


}
