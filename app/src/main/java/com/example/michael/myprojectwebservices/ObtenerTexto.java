package com.example.michael.myprojectwebservices;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by michael on 15/11/2017.
 */


public class ObtenerTexto extends AppCompatActivity implements View.OnKeyListener{

    static final String TAG = ObtenerTexto.class.getName();

    TextView txtTexto;
    EditText edTexto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba);
        txtTexto = (TextView) findViewById(R.id.txtTexto);

        edTexto = (EditText) findViewById(R.id.edTexto);


        TextWatcher KeyLogTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    txtTexto.setText("texto es "+ edTexto.getText());

                }
            }
        };
        edTexto.addTextChangedListener(KeyLogTextWatcher);



        /*edTexto.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if((event.getAction()==KeyEvent.ACTION_DOWN)&&(keyCode==KeyEvent.KEYCODE_ENTER)){
                    Toast.makeText(ObtenerTexto.this, "Lo digitado fue "+edTexto.getText(), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });*/

        /*
        txtTexto.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if((event.getAction()==KeyEvent.ACTION_DOWN)&&(keyCode==KeyEvent.KEYCODE_ENTER)){

                }

                return false;
            }
        });*/

    }
    void onViewTextChanged(AccessibilityEvent accessibilityEvent, AccessibilityNodeInfo accessibilityNodeInfo) {
        List text = accessibilityEvent.getText();
        CharSequence latestText = (CharSequence) text.get(0);
        Log.e(TAG, "dentro de onViewTextChanged" + latestText.toString());
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
     if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                (keyCode == KeyEvent.KEYCODE_ENTER)) {
            Toast.makeText(ObtenerTexto.this,"YOU CLICKED ENTER KEY", Toast.LENGTH_LONG).show();
            Log.i(TAG, "dentro de onKey");

            return true;
        }else{

            return false;}

    }

/*
    @Override
    public int getInputType() {
        return 0;
    }

    @Override
    public boolean onKeyDown(View view, Editable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyUp(View view, Editable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyOther(View view, Editable text, KeyEvent event) {
        return false;
    }

    @Override
    public void clearMetaKeyState(View view, Editable content, int states) {

    }*/
    public void saludar(){
        System.out.println("Hola idiota");
    }

    public void saludarasasChupon(){

    }
}

