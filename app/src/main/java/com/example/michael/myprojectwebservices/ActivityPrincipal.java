package com.example.michael.myprojectwebservices;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by michael on 23/11/2017.
 */

public class ActivityPrincipal extends AppCompatActivity {
    Button btnPerfil, btnSalir;
    String id_per;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        btnPerfil = (Button) findViewById(R.id.btnPerfil);
        btnSalir = (Button) findViewById(R.id.btnSalir);
        Intent recibir = getIntent();
        String id = recibir.getStringExtra("id");
        id_per = id;


        //Toast.makeText(this, "Perfil " + "usuario con id "+  id_per, Toast.LENGTH_SHORT).show();

        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityPrincipal.this, Perfil.class);
                i.putExtra("id", id_per);
                startActivity(i);

            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(Perfil.this, "nombre es "+edNombre.getText().toString(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ActivityPrincipal.this, Login.class);
                startActivity(i);
                finish();

            }
        });
    }
}
