package com.example.michael.myprojectwebservices;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class RegistrarPersona extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
//Implementamos el Response Para trabajar con Volley  y Json
    EditText edNombre, edApellido, edContrasenia, edUsuario;
    Button btnRegistrar;
    ProgressDialog progreso;
    Boolean validar;
    //Para ver el progreso en caso de que tarde la petición al webservices

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        edNombre = (EditText) findViewById(R.id.etNombre);
        edApellido = (EditText) findViewById(R.id.etApellido);
        edUsuario = (EditText) findViewById(R.id.etUsuario);
        edContrasenia = (EditText) findViewById(R.id.etContrasenia);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);

        request = Volley.newRequestQueue(RegistrarPersona.this);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre, apellido, usuario, contrasenia;
                nombre = edNombre.getText().toString();
                apellido = edApellido.getText().toString();
                usuario = edUsuario.getText().toString();
                contrasenia = edContrasenia.getText().toString();
                if (!nombre.isEmpty() && !apellido.isEmpty() && !usuario.isEmpty() && !contrasenia.isEmpty()) {
                    cargarWerService();
                } else {
                    Toast.makeText(RegistrarPersona.this, "Falta digitar un campo", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void cargarWerService() {
        progreso = new ProgressDialog(RegistrarPersona.this);
        progreso.setMessage("Cargando..");
        progreso.show();

        String url = "http://pruebawebser.webcindario.com/insertarPersona.php?nombre="+edNombre.getText().toString()+"&usuario="+edUsuario.getText().toString()+"&contrasenia="+edContrasenia.getText().toString()+"&apellido="+edApellido.getText().toString();

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);



    }


    //si todo va bien, viene aquí, y se agrega el jsonObjectRequest dentro del request, nos envia al metodo OnResponse
    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(RegistrarPersona.this, "Se ha registrado correctamente", Toast.LENGTH_SHORT).show();
        progreso.hide();
        edNombre.setText("");
        edApellido.setText("");
        edContrasenia.setText("");
        edUsuario.setText("");
        Intent i = new Intent(RegistrarPersona.this, Login.class);
        startActivity(i);

    }

//si nos retorna error viene aqui
    @Override
    public void onErrorResponse(VolleyError error) {
    progreso.hide();
        Toast.makeText(RegistrarPersona.this, "No se pudo registrar", Toast.LENGTH_SHORT).show();
        Log.i("Error", error.toString());
    }

}
