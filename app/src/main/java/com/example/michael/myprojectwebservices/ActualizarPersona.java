package com.example.michael.myprojectwebservices;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class ActualizarPersona extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    //Implementamos el Response Para trabajar con Volley  y Json

    //ProgressDialog progreso;
    //Para ver el progreso en caso de que tarde la petición al webservices

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        request = Volley.newRequestQueue(ActualizarPersona.this);
    }




    public void cargarWerService(String nombre, String usuario, String contrasenia, String apellido, int id_persona) {
//        progreso = new ProgressDialog(ActualizarPersona.this);
  //      progreso.setMessage("Cargando..");
    //    progreso.show();
        //pero tampoco me dio...  innecesario lo de la otra clase...dejame ver...
        //ubicame en el codigo php de peticon respuesta, de actualizar o de select?

        String url = "http://pruebawebser.webcindario.com/actualizarPersona.php?nombre="+nombre+"&usuario="+usuario+"&contrasenia="+contrasenia+"&apellido="+apellido+"&id_persona="+id_persona;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);



    }


    //si todo va bien, viene aquí, y se agrega el jsonObjectRequest dentro del request, nos envia al metodo OnResponse
    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(ActualizarPersona.this, "Se ha editado correctamente", Toast.LENGTH_SHORT).show();
        //progreso.hide();
        //edNombre.setText("");
        //edApellido.setText("");
        //edContrasenia.setText("");
        //edUsuario.setText("");
        //Intent i = new Intent(ActualizarPersona.this, Login.class);
        //startActivity(i);
//Recuerda que esta clase ActualizarPersona no la estoy usando.. porque en principio, tampoco me funcionó
    }

    //si nos retorna error viene aqui
    @Override
    public void onErrorResponse(VolleyError error) {
        //progreso.hide();
        Toast.makeText(ActualizarPersona.this, "No se pudo registrar", Toast.LENGTH_SHORT).show();
        Log.i("Error", error.toString());
    }

}
