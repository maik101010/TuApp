package com.example.michael.myprojectwebservices;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.michael.myprojectwebservices.Datos.Persona;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by michael on 08/11/2017.
 */

public class Login extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    EditText edUsuario, edContrasenia;
    Button btnIngresar, btnRegistrar;
    ProgressDialog progreso;
    JsonObjectRequest jsonObjectRequest;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edUsuario = (EditText) findViewById(R.id.etUsuario);
        edContrasenia = (EditText) findViewById(R.id.etContrasenia);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);


    requestQueue = Volley.newRequestQueue(Login.this);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //consultaPass("http://pruebawebser.webcindario.com/ingresar.php?nombre="+edUsuario.getText().toString());
                cargarWerbservices();
            }


        });


        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, RegistrarPersona.class);
                startActivity(i);
            }
        });

    }

    /*private void consultaPass(String url) {
        Log.i("url", ""+url);
        requestQueue = Volley.newRequestQueue()

    }*/


    @Override
    public void onResponse(JSONObject response) {
        progreso.hide();

        Persona us = new Persona();

        JSONArray json = response.optJSONArray("usuario");
        JSONObject jsonObject = null;

       try {
           jsonObject = json.getJSONObject(0);
           us.setContrasenia(jsonObject.getString("contrasenia"));


            if (us.getContrasenia().equals(edContrasenia.getText().toString().toLowerCase())){
                Intent i = new Intent(Login.this, ActivityPrincipal.class);
                us.setId(jsonObject.getInt("id_persona"));
                String id = String.valueOf(us.getId());

                //String idString = id);
                i.putExtra("id", id);



                startActivity(i);

            }else{
                Toast.makeText(Login.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
            }




        } catch (JSONException e) {
            e.printStackTrace();
           //Toast.makeText(getApplicationContext(),"El usuario no existe en la base de datos",Toast.LENGTH_LONG).show();

       }catch (NullPointerException e){
           Toast.makeText(getApplicationContext(),"El usuario no existe en la base de datos",Toast.LENGTH_LONG).show();
       }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progreso.hide();
        Toast.makeText(Login.this, "EL usuario no existe en la base de datos", Toast.LENGTH_SHORT).show();
        Log.i("Error", error.toString());
    }


    private void cargarWerbservices() {
        progreso = new ProgressDialog(Login.this);
        progreso.setMessage("Consultando..");
        String url = "http://pruebawebser.webcindario.com/ingresar.php?usuario="+edUsuario.getText().toString();

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        requestQueue.add(jsonObjectRequest);

    }




}

